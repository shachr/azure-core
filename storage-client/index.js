/**
 * Created by shachar on 3/6/15.
 */
var azure = require('azure-storage');
var util  = require('util');

var entityGen = azure.TableUtilities.entityGenerator;
module.exports = AzureClient;

function AzureClient( account, secretKey ) {
    this.$storageClient = azure.createTableService(account, secretKey);
}

AzureClient.prototype = {
    table: function(tableName, partitionKey){
        return new AzureTableClient(this.$storageClient, tableName,partitionKey);
    }
};


// docs: https://github.com/Azure/azure-content/blob/master/articles/storage-nodejs-how-to-use-table-storage.md
function AzureTableClient( storageClient, tableName, partitionKey ){
    this.$storageClient = storageClient;
    this.$tableName = tableName;
    this.$partitionKey = partitionKey;
}

AzureTableClient.prototype = {

    ensureTable: function( callback){
        this.$storageClient.createTableIfNotExists(this.$tableName, function tableCreated() {
            if(callback)
                return callback.apply(this, arguments);
        });
        return this;
    },

    query: function(){
        return new AzureQueryBuilder(this.$storageClient, this.$tableName);
    },

    insert: function(item, callback) {

        if(!item)return callback(new Error("item is not defined"));
        if(!item._id)return callback(new Error("_id is not defined"));

        // use entityGenerator to set types
        // NOTE: RowKey must be a string type, even though
        // it contains a GUID in this example.
        var itemDescriptor = {
            PartitionKey: entityGen.String(self.$partitionKey),
            RowKey: entityGen.String(item._id)
        };

        // wrap as a type and insert each item field to the table
        for( var key in item ) {
            if(key == "PartitionKey")continue; // no, no!
            if(key == "RowKey")continue; // no, no!
            if(key == "_id")continue; // no, no!
            itemDescriptor[key] = {'_': item[key]};
        }


        this.$storageClient.insertEntity(self.$tableName, itemDescriptor, function entityInserted(error) {
            if(error)
                return callback(error);

            callback(null);
        });
    },

    update: function(item, callback) {

        if(!item)return callback(new Error("item is not defined"));
        if(!item._id)return callback(new Error("_id is not defined"));

        self = this;
        self.$storageClient.retrieveEntity(self.$tableName, self.$partitionKey, rKey,
            function entityQueried(error, entity) {
                if(error)
                    return callback(error);
                
                // update doc.
                for(var key in item){
                    if(key == "PartitionKey")continue; // no, no!
                    if(key == "RowKey")continue; // no, no!
                    if(key == "_id")continue; // no, no!

                    var field = entity[ key ] = entity[ key ] || {};
                    field._ = item[ key ]
                };

                self.$storageClient.updateEntity(self.$tableName, entity, function entityUpdated(error) {
                    if(error) {
                        callback(error);
                    }
                    callback(null);
                });
            }
        );
    }
};



function AzureQueryBuilder( storageClient, tableName ){
    this.$storageClient = storageClient;
    this.$tableName = tableName;
    azure.TableQuery.call(this);
}
util.inherits(AzureQueryBuilder,azure.TableQuery);

AzureQueryBuilder.prototype.find = function( callback ){
    this.$storageClient.queryEntities(this.$tableName, this, null, function entitiesQueried(error, result) {
        return callback(error, result ? result.entries : null);
    });
};