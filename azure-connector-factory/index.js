/**
 * Created by shachar on 9/2/15.
 */
var azure = require('azure');

//todo:  move these options to a configuration file (app settings?).
var topicName = 'soa';
var subscriptionName = 'sub';
var ruleName = 'ServiceConnectorFilter';

var topicOptions = {
    MaxSizeInMegabytes: '5120',
    DefaultMessageTimeToLive: 'PT1M'
};

var ConnectorFactory = module.exports = {
    create: function( connectionString, sessionId, callback ){

        var retryOperations = new azure.ExponentialRetryPolicyFilter();
        // signature: configOrNamespaceOrConnectionString, accessKey, issuer, acsNamespace, host, authenticationProvider
        var serviceBusService = azure.createServiceBusService(connectionString).withFilter(retryOperations);

        serviceBusService.createTopicIfNotExists(topicName, topicOptions, function(error){

            if(error)
                return callback(error);


            // Topic was created or exists
            serviceBusService.createSubscription(topicName, subscriptionName, function (error){
                if(error)
                    return callback(error);

                // subscription created
                rule.create( sessionId, function(error){
                    if(error) return callback(error);
                    // return the public api of the connector
                    var api = {
                        send: function( path, params, options ) {
                            serviceBusService.sendTopicMessage(topicName, { path: path, body: params, headers: { sessionId: sessionId} }, function(error) {
                                if (error) {
                                    console.log(error);
                                }
                            });
                        },

                        recieve: function( handler, errorHandler ){
                            serviceBusService.receiveSubscriptionMessage(topicName, ruleName, { isPeekLock: true }, function(error, lockedMessage){
                                if(!error){
                                    // Message received and locked
                                    handler.call(lockedMessage,
                                        function(err){
                                            // on complete handler that will delete the message.
                                            if(err){
                                                serviceBusService.unlockMessage(lockedMessage, function(unlockError){
                                                    if(unlockError)
                                                        errorHandler.call(null, unlockError ); //todo: retry or call an error handler

                                                })
                                            }
                                            else {
                                                serviceBusService.deleteMessage(lockedMessage, function (deleteError) {
                                                    if(deleteError)
                                                        errorHandler.call(null, deleteError); //todo: retry or call an error handler
                                                });
                                            }
                                        }
                                    );
                                }
                            });
                        }
                    };

                    callback( null, api );
                });

            });
        });
    }
};



var rule={
    deleteDefault: function(callback){
        serviceBusService.deleteRule(
            topicName,
            subscriptionName,
            azure.Constants.ServiceBusConstants.DEFAULT_RULE_NAME,
            callback);
    },

    create: function( sessionId, callback ){
        var ruleOptions = {
            sqlExpressionFilter: 'sessionID = \''+ sessionId  + '\''
        };

        rule.deleteDefault(
            function( error ){
                if(error) return callback();

                serviceBusService.createRule(
                    topicName,
                    subscriptionName,
                    ruleName,
                    ruleOptions,
                    callback);

                callback();
            }
        );


    }
};